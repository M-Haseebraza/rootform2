import React from 'react'
import Rules from './components/Rules'
import Slider from './components/Slider'

export default function Form() {
    return (
        <div className='container m-0 p-0 form_container'>
            {/* <Slider /> */}
            <Rules />

        </div>
    )
}
