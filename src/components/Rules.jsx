import React from 'react';
import whitelogo from '../images/white-logo.png'
import Slider from './Slider';
import pic1 from "../images/pic1.jpg"
import pic2 from "../images/pic2.jpg"
export default function Rules() {
    return (<>
        <div style={{ backgroundColor: "#8D398F" }}>
            <div style={{ backgroundColor: "rgba(1, 0, 0,0.6)", height: "65px", color: "white" }} className='rules_header py-2 px-3'>
                <img height={50} src={pic2} alt="" />
                <div className='d-flex mt-2 text-center'>
                    <h2>Roots IVY </h2>
                </div>
                <img height={50} src={whitelogo} alt="" />
            </div>
        </div>
        <div className='rules_container'>
            <Slider />
            <div style={{ backgroundColor: "rgba(1, 0, 0, 0.6)" }} className='text-center  mt-3 py-3'> <h2>IVY Bilingual</h2>  <h1>Declamation</h1> <h3> ---Contest--- </h3><br /><h3>(IBDC) 2022</h3></div>
            <br />
            <div className=''>
                <span><b><u>Eligibility:</u></b></span>
                <ol><li><span>All the chartered Public and Private Sector Schools of Faisalabad functional at the time of announcement of the contest are eligible for the Declamation Contest.</span></li><li><span>Male and female regular students, both are eligible to participate in both languages that is Urdu and English.</span></li></ol>
            </div>
            <br />
            <div style={{
                backgroundColor: "antiquewhite",
                color: "#8D398F"
            }} className='cart row mx-1 mt-3'>
                <div className="col-md-5">
                    <img style={{ width: "100%" }} src={pic1} alt="" />
                </div>
                <div className="col-md-7 mt-5">
                    <span><b><u>Rules and Regulations:</u></b></span>
                    <ol><li>Minimum one and maximum two teams may represent its institution.&nbsp;</li><li>Team members will appear in their uniforms.</li>
                        <li>Institute letterhead is compulsory confirming enrollment of students.&nbsp;</li>
                        <li>Only Matric, IGCSE O-levels , IB MYP students can participate.&nbsp;</li>
                        <li>Three student observers per institute can participate.&nbsp;<br /></li>
                        <li>Each speaker will have four minutes to speak for or against the topic, <b>First</b> bell will be rung after three minutes, while second and final will be rung after four minutes. On the <b>second</b> bell speaker will have to leave the rostrum. Failure to do so will lead to negative marking (5 marks per minute).</li>
                        <li>The judges can disqualify any speaker if found using examples from the film industry/ television programs, unparliamentary language, derogatory/ sectarian remarks and reading from paper/ notes or cards.&nbsp;</li><li>The top three winners each in English and Urdu, shall be awarded <b>CASH PRIZES</b>.</li><li><span>A panel of judges shall be decided by the ROOTS IVY educational organizers, whose decision will be final and unchallengeable at any level including court of law.&nbsp;</span></li></ol>

                </div>
            </div>
            <br />
            <div style={{ backgroundColor: "rgba(1, 0, 0, 0.6)" }} className='cart row'>
                <div >
                    <span><b><u>Procedure:</u></b></span> <br />
                    Registration for single student for each category <br />
                    1. English : 1000/- Rupees <br />
                    2. Urdu : 1000/- Rupees
                </div>
                <br />
                <div>
                    Registration for 1 Team (Including 2 members one for English and one for Urdu) : 2000/- Rupees <br />
                    LAST DATE TO APPLY :<b>21-03-2022(Monday)</b>
                </div>
                <br />
                <div className=''>
                    <span><b><u>Venue:</u></b></span> <br />
                    Roots IVY Educational Complex ( East Canal Road, near FCCI Faisalabad)
                </div>
                <button className='register_now mt-3'>Registor Now</button>
            </div>
        </div>
    </>
    )
}
