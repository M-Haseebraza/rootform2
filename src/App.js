import logo from './logo.svg';
import './App.css';
import Form from './Form';
function App() {
  return (
    <div className="App">
      <div className='opacity_div'>
        <Form />
      </div>

    </div>
  );
}

export default App;
